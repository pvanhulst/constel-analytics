Visual Tools for Web Analytics
==============================
This project is a Master Thesis. It aims at building a Symfony2 bundle allowing a better visualisation of data from various sources.
Data will be preprocessed in order to allow easy segmentation of the activity.

PLAN
========================
- Set up the UI : up to 22th of november
- Set up D3 diagrams : up to 27th of november
- Set up Entities : up to ??
- Set up Google Analytics Requests :
