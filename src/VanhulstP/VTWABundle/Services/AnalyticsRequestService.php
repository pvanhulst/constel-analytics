<?php

namespace VanhulstP\VTWABundle\Services;

use HappyR\Google\ApiBundle\Services\AnalyticsService;
use HappyR\Google\AnalyticsBundle\CacheServices\CacheInterface;
use HappyR\Google\AnalyticsBundle\Entity\GoogleApiReportToken;

/**
 * Class AnalyticsRequestService
 *
 * This service sends specific requests to Google Analytics
 */
class AnalyticsRequestService
{
    protected $analytics;
    private $tokenService;
    private $config;
    protected $cache;
    protected $jsonCache;
    protected $jsonCachePrefix = 'json-cache-';
    protected $max_pagelevel;
    protected $startDate;
    protected $endDate;
    protected $segment;
    protected $distinction;

    public function __construct(
        AnalyticsService $analyticsService,
        \HappyR\Google\AnalyticsBundle\Services\TokenService $tokenService,
        CacheInterface $cache,
        array $config,
        $jsonCache,
        $max_pagelevel,
        $distinction
    )
    {
        $this->analytics = $analyticsService;
        $this->tokenService = $tokenService;
        $this->config = $config;
        $this->cache = $cache;
        $this->jsonCache = $jsonCache;
        $this->max_pagelevel = $max_pagelevel;
        $this->distinction = $distinction;
    }

    /**
     * Returns true if we have a access token
     *
     * @return bool
     */
    private function hasAccessToken()
    {
        $token = $this->tokenService->getToken();
        if (!$token) {
            return false;
        }

        $this->analytics->client->setAccessToken($token);

        return $this->analytics->client->getAccessToken();
    }

    /**
     * The access token might get refreshed so we need to save it after each request
     */
    private function saveAccessToken()
    {
        $token = $this->analytics->client->getAccessToken();

        //save token
        $this->tokenService->setToken($token);
    }


    private function in_array_r($needle, $haystack, $parent = null)
    {
        $found = null;
        foreach ($haystack as $key => $item) {
            if ($item === $needle) {
                if ($parent || $parent === 0)
                    $found = $parent;
                else $found = $key;
                break;
            } elseif (is_array($item)) {
                if ($parent !== null || $parent === 0) {
                    $found = $this->in_array_r($needle, $item, $parent);
                } else $found = $this->in_array_r($needle, $item, $key);
                if ($found || $found === 0) {
                    break;
                }
            }
        }
        return $found;
    }


    private function parseDimensionsOrMetrics(array $dimensionsOrMetrics)
    {
        $parsedDimensionsOrMetrics = '';
        $dumbCounter = 0;

        foreach ($dimensionsOrMetrics as $dimensionOrMetric) {
            if ($dumbCounter == 0) {
                $parsedDimensionsOrMetrics = 'ga:' . $dimensionOrMetric;
                $dumbCounter++;
            } else
                $parsedDimensionsOrMetrics = $parsedDimensionsOrMetrics . ', ga:' . $dimensionOrMetric;
        }

        return $parsedDimensionsOrMetrics;
    }

    public function saveJson($jsonType, $jsonResult, $startDate, $endDate, $segment, $metric = null, $maxPages = null, $maxInteractions = null, $filter = null)
    {
        $name = md5($startDate . $endDate . $segment . $metric . $maxPages . $maxInteractions . $filter);
        $filename = $this->jsonCachePrefix . $jsonType . '-' . $name . '.json';

        return @file_put_contents($this->jsonCache . $filename, $jsonResult);
    }

    public function getSegments()
    {
        if (!$this->hasAccessToken()) {
            return 0;
        }

        try {
            $segments = $this->analytics->management_segments
                ->listManagementSegments();

        } catch (apiServiceException $e) {
            print 'There was an Analytics API service error '
                . $e->getCode() . ':' . $e->getMessage();

        } catch (apiException $e) {
            print 'There was a general API error '
                . $e->getCode() . ':' . $e->getMessage();
        }

        $segmentsList = array();
        foreach ($segments->getItems() as $segment) {
            array_push($segmentsList, array("name" => $segment->getName(), "id" => $segment->getSegmentId()));
        }

        return $segmentsList;
    }

    public function getFilters($startDate, $endDate, $dimension, $segment, $metric)
    {
        $filtersDimensions = array($dimension);
        $filtersMetrics = array($metric);
        $filtersMax = 50;
        $filtersSort = '-ga:' . $metric;

        $filtersRows = $this->getAnalyticsData($filtersDimensions, $filtersMetrics, $filtersMax, $filtersSort, null, $startDate, $endDate, $segment);

        return $filtersRows;
    }

    public function getAnalyticsData(array $dimensions = null, array $metrics, $maxResults = 10000, $sort = null, $filters = null, $startDate = null, $endDate = null, $segment = null)
    {

        if (!$this->hasAccessToken()) {
            return 0;
        }

        $optParams = array();

        if ($dimensions)
            $optParams['dimensions'] = $this->parseDimensionsOrMetrics($dimensions);
        if ($sort)
            $optParams['sort'] = $sort;
        if ($filters)
            $optParams['filters'] = $filters;
        if ($segment)
            $optParams['segment'] = $segment;
        $optParams['max-results'] = $maxResults;

        try {
            $results = $this->analytics->data_ga->get(
                'ga:' . $this->config['profile_id'],
                $startDate,
                $endDate,
                $this->parseDimensionsOrMetrics($metrics),
                $optParams
            );
            $rows = $results->getRows();
        } catch (\Google_AuthException $e) {
            $rows = 0;
        }

        //save access token
        $this->saveAccessToken();

        return $rows;
    }

    public function getPagesByPath($startDate, $endDate, $segment, $metric, $max, $filter)
    {
        // Prepares the list
        $pagesDimensions = array('pageTitle', 'pagePath');
        $pagesMetrics = array($metric);
        $pagesMax = $max ? $max : 1000;
        $pagesSort = '-ga:' . $metric;
        $pagesRows = $this->getAnalyticsData($pagesDimensions, $pagesMetrics, $pagesMax, $pagesSort, $filter, $startDate, $endDate, $segment);

        $groupsList = array();
        $pagesList = array();
        foreach ($pagesRows as $page) {
            $fullPath = pathinfo($page[1]);
            $explodedPath = explode('/', $fullPath['dirname']);
            $path = '';
            for ($i = 1; $i <= $this->max_pagelevel; $i++) {
                if (isset($explodedPath[$i]))
                    $path .= $explodedPath[$i];
            }
            $group = array_search($path, $groupsList);
            if ($group === false) {
                array_push($groupsList, $path);
                end($groupsList);
                $group = key($groupsList);
            }

            array_push($pagesList, array("name" => $page[0], "uri" => array($page[1]), "group" => $group, "value" => intval($page[2])));
        }

        return $pagesList;
    }

    public function getPagesByTitle($startDate, $endDate, $segment, $metric, $max, $filter)
    {
        // Prepares the PageTitles list
        $titlesDimensions = array('pageTitle');
        $titlesMetrics = array($metric);
        $titlesMax = $max ? $max : 1000;
        $titlesSort = '-ga:' . $metric;
        $titlesRows = $this->getAnalyticsData($titlesDimensions, $titlesMetrics, $titlesMax, $titlesSort, $filter, $startDate, $endDate, $segment);

        // Prepares the PagePaths list
        $urlDimensions = array('pagePath', 'pageTitle');
        $urlMetrics = array($metric);
        $urlMax = 10000;
        $urlSort = '-ga:' . $metric;
        $urlRows = $this->getAnalyticsData($urlDimensions, $urlMetrics, $urlMax, $urlSort, $filter, $startDate, $endDate, $segment);

        // Prepares the Pages list
        $pagesRows = array();
        foreach ($urlRows as $key => $url) {
            if ($this->in_array_r($url[1], $titlesRows) !== null) {
                if ($this->in_array_r($url[1], $pagesRows) !== null) {
                    $index = $this->in_array_r($url[1], $pagesRows);
                    $pagesRows[$index][2] = $pagesRows[$index][2] + $url[2];
                    array_push($pagesRows[$index][0], $url[0]);
                } else array_push($pagesRows, array(array($url[0]), $url[1], $url[2]));
            }
        }

        $groupsList = array();
        $pagesList = array();
        foreach ($pagesRows as $page) {
            $fullPath = pathinfo($page[0][0]);
            $explodedPath = explode('/', $fullPath['dirname']);
            $path = '';
            for ($i = 1; $i <= $this->max_pagelevel; $i++) {
                if (isset($explodedPath[$i]))
                    $path .= $explodedPath[$i];
            }
            $group = array_search($path, $groupsList);
            if ($group === false) {
                array_push($groupsList, $path);
                end($groupsList);
                $group = key($groupsList);
            }

            array_push($pagesList, array("name" => $page[1], "uri" => $page[0], "group" => $group, "value" => intval($page[2])));
        }
        return $pagesList;
    }

    public function getPages($startDate, $endDate, $segment, $metric, $max, $filter)
    {
        if ($this->distinction == "path")
            return $this->getPagesByPath($startDate, $endDate, $segment, $metric, $max, $filter);
        else return $this->getPagesByTitle($startDate, $endDate, $segment, $metric, $max, $filter);

    }

    public function getInteractions(array $pagesList, $startDate, $endDate, $segment, $metric, $max, $filter)
    {
        $interactionsDimensions = array('previousPagePath', 'pagePath');
        $interactionsMetrics = array($metric);
        $interactionsMax = $max ? $max : 10000;
        $interactionsSort = '-ga:' . $metric;

        $rows = $this->getAnalyticsData($interactionsDimensions, $interactionsMetrics, $interactionsMax, $interactionsSort, $filter, $startDate, $endDate, $segment);
        $number = count($rows);
        $interactionsList = array();

        foreach ($rows as $interaction) {
            if ($this->in_array_r($interaction[0], $pagesList) !== null && $this->in_array_r($interaction[1], $pagesList) !== null) {
                $source = $this->in_array_r($interaction[0], $pagesList);
                $target = $this->in_array_r($interaction[1], $pagesList);
                $weight = $interaction[2];
                if ($source != $target) {
                    if (count($interactionsList)) {
                        $isInArray = false;
                        foreach ($interactionsList as $rInteraction) {
                            if ($rInteraction['source'] == $source && $rInteraction['target'] == $target) {
                                $rInteraction['value'] = $rInteraction['value'] + $weight;
                                $isInArray = true;
                            }
                        }
                        if (!$isInArray)
                            array_push($interactionsList, array("source" => $source, "target" => $target, "value" => $weight));
                    } else array_push($interactionsList, array("source" => $source, "target" => $target, "value" => $weight));
                }
            }
        }
        return $interactionsList;
    }

    public function getVisits($startDate = null, $endDate = null, $segment = null, $goal1, $goal2)
    {
        $visitsDimensions = array("visitCount", "visitLength");
        $visitsMetrics = array("visitors", "goal" . $goal1 . "Completions", "goal" . $goal2 . "Completions", "pageviewsPerVisit");
        $visitsMax = 10000;
        $visitsSort = '-ga:visitors';

        $rows = $this->getAnalyticsData($visitsDimensions, $visitsMetrics, $visitsMax, $visitsSort, '', $startDate, $endDate, $segment);

        return $rows;
    }
}