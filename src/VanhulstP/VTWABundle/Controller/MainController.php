<?php

namespace VanhulstP\VTWABundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use HappyR\Google\AnalyticsBundle\Entity\GoogleApiReportToken;

class MainController extends Controller
{
    public function indexAction()
    {
        return $this->render('VTWABundle:Default:index.html.twig');
    }

    public function interactionsAction()
    {
        $request = $this->get('request');
        $startDate = (strtotime($request->query->get('start')) == true ? $request->query->get('start') : date('Y-m-d', time() - 86400 * 20));
        $endDate = (strtotime($request->query->get('end')) == true ? $request->query->get('end') : date('Y-m-d', time() - 86400));
        $segment = $request->query->get('segment');
        $filter = $request->query->get('filter');
        $selectedFilter = $request->query->get('selectedFilter') ? $request->query->get('selectedFilter') : 'country';
        $metric = $request->query->get('metric') ? $request->query->get('metric') : 'pageviews';
        $maxPages = $request->query->get('maxPages') ? $request->query->get('maxPages') : 100;
        $maxInteractions = $request->query->get('maxInteractions') ? $request->query->get('maxInteractions') : 1000;
        $fileName = md5($startDate . $endDate . $segment . $metric . $maxPages . $maxInteractions . $filter);

        $filePath = $this->get('service_container')->getParameter('vtwa.json_cache_path') . 'json-cache-interactions-' . $fileName . '.json';
        $siteName = $this->get('service_container')->getParameter('vtwa.site_name');

        $analytics = $this->get('vtwa.analytics_request');

        $segmentsList = $analytics->getSegments();
        $filtersList = $analytics->getFilters($startDate, $endDate, $selectedFilter, $segment, $metric);

        if (file_exists($filePath) && $endDate != date('Y-m-d')) {
            $jsonResult = file_get_contents($filePath);
        } else {
            // List the 100 most viewed pages as a base for the Force-Directed Diagram
            $pagesList = $analytics->getPages($startDate, $endDate, $segment, $metric, $maxPages, $filter);

            // Get all the interactions related to the previous pages
            $interactionsList = $analytics->getInteractions($pagesList, $startDate, $endDate, $segment, $metric, $maxInteractions, $filter);

            $arrayResult = array(
                "nodes" => $pagesList,
                "links" => $interactionsList
            );
            $jsonResult = json_encode($arrayResult);

            // Save the json result
            $analytics->saveJson('interactions', $jsonResult, $startDate, $endDate, $segment, $metric, $maxPages, $maxInteractions, $filter);
        }

        return $this->render('VTWABundle:Default:interactions.html.twig', array(
            'json' => $jsonResult,
            'segments' => $segmentsList,
            'filters' => $filtersList,
            'currentSegment' => $segment,
            'currentMetric' => $metric,
            'currentStart' => $startDate,
            'currentEnd' => $endDate,
            'siteName' => $siteName,
            'currentMaxPages' => $maxPages,
            'currentMaxInteractions' => $maxInteractions,
            'currentFilter' => $filter,
            'selectedFilter' => $selectedFilter
        ));
    }

    public function visitsAction()
    {
        $request = $this->get('request');
        $startDate = (strtotime($request->query->get('start')) == true ? $request->query->get('start') : date('Y-m-d', time() - 86400 * 20));
        $endDate = (strtotime($request->query->get('end')) == true ? $request->query->get('end') : date('Y-m-d', time() - 86400));
        $segment = $request->query->get('segment');
        $fileName = md5($startDate . $endDate . $segment);

        $filePath = $this->get('service_container')->getParameter('vtwa.json_cache_path') . 'json-cache-visits-' . $fileName . '.json';
        $siteName = $this->get('service_container')->getParameter('vtwa.site_name');

        $analytics = $this->get('vtwa.analytics_request');

        $segmentsList = $analytics->getSegments();

        if (file_exists($filePath) && $endDate != date('Y-m-d')) {
            $jsonResult = file_get_contents($filePath);
        } else {
            $pagesList = $analytics->getVisits($startDate, $endDate, $segment, 1, 3);

            $jsonResult = json_encode($pagesList);

            // Save the json result
            $analytics->saveJson('visits', $jsonResult, $startDate, $endDate, $segment);
        }

        return $this->render('VTWABundle:Default:visits.html.twig', array(
            'json' => $jsonResult,
            'segments' => $segmentsList,
            'currentSegment' => $segment,
            'currentStart' => $startDate,
            'currentEnd' => $endDate,
            'siteName' => $siteName
        ));
    }

    public function ajaxAction()
    {
        $request = $this->get('request');
        $startDate = (strtotime($request->query->get('start')) == true ? $request->query->get('start') : date('Y-m-d', time() - 86400 * 20));
        $endDate = (strtotime($request->query->get('end')) == true ? $request->query->get('end') : date('Y-m-d', time() - 86400));
        $segment = $request->query->get('segment');
        $filter = $request->query->get('filter');
        $metric = $request->query->get('metric') ? $request->query->get('metric') : 'pageviews';
        $maxPages = $request->query->get('maxPages') ? $request->query->get('maxPages') : 100;
        $maxInteractions = $request->query->get('maxInteractions') ? $request->query->get('maxInteractions') : 1000;
        $fileName = md5($startDate . $endDate . $segment . $metric . $maxPages . $maxInteractions . $filter);

        $filePath = $this->get('service_container')->getParameter('vtwa.json_cache_path') . 'json-cache-interactions-' . $fileName . '.json';
        $siteName = $this->get('service_container')->getParameter('vtwa.site_name');

        $analytics = $this->get('vtwa.analytics_request');

        if (file_exists($filePath) && $endDate != date('Y-m-d')) {
            $jsonResult = file_get_contents($filePath);
        } else {
            // List the 100 most viewed pages as a base for the Force-Directed Diagram
            $pagesList = $analytics->getPages($startDate, $endDate, $segment, $metric, $maxPages, $filter);

            // Get all the interactions related to the previous pages
            $interactionsList = $analytics->getInteractions($pagesList, $startDate, $endDate, $segment, $metric, $maxInteractions, $filter);

            $arrayResult = array(
                "nodes" => $pagesList,
                "links" => $interactionsList
            );
            $jsonResult = json_encode($arrayResult);

            // Save the json result
            $analytics->saveJson('interactions', $jsonResult, $startDate, $endDate, $segment, $metric, $maxPages, $maxInteractions, $filter);
        }

        return $this->render('VTWABundle:Helpers:ajax.html.twig', array(
            'json' => $jsonResult
        ));
    }
}
