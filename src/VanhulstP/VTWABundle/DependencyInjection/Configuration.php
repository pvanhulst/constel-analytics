<?php

namespace VanhulstP\VTWABundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('vtwa');

        $rootNode
            ->children()
            ->scalarNode('json_cache_path')
            ->defaultValue('%kernel.root_dir%/var/vtwa/storage')
            ->isRequired()
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('site_name')
            ->isRequired()
            ->cannotBeEmpty()
            ->end()
            ->integerNode('max_pagelevel')
            ->defaultValue('1')
            ->end()
            ->scalarNode('distinction')
            ->defaultValue('title')
            ->end();

        return $treeBuilder;
    }
}
